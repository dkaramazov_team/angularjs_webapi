﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APM.WebAPI.Models;
using System.Web.Http.Cors;
using System.Web.Http.OData;
using System.Web.Http.Description;

namespace Pluralsight.WebAPI.Controllers
{
	[EnableCors("http://localhost:52013", "*", "*")]
	public class ProductsController : ApiController
    {
        // GET: api/Products
		[EnableQuery()]
		[ResponseType(typeof(Product))]
        public IHttpActionResult Get()
        {
			var productRepository = new ProductRepository();
			return Ok(productRepository.Retrieve().AsQueryable());
        }

		// GET: api/Products/search=GDN
		[ResponseType(typeof(Product))]
		public IEnumerable<Product> Get(string search)
		{
			var productRepository = new ProductRepository();
			var products = productRepository.Retrieve();
			return products.Where(p => p.ProductCode.Contains(search));
		}

		// GET: api/Products/5
		[ResponseType(typeof(Product))]
		[Authorize()]
		public IHttpActionResult Get(int id)
		{
			Product product;
			var productRepository = new ProductRepository();
			var products = productRepository.Retrieve();
			if (id > 0)
			{
				products = productRepository.Retrieve();
				product = products.FirstOrDefault(p => p.ProductId == id);
				if (product == null) {
					return NotFound();
				}
			}
			else {
				product = productRepository.Create();
			}
			return Ok(product);
        }

		// POST: api/Products
		[ResponseType(typeof(Product))]
		public IHttpActionResult Post([FromBody]Product product)
        {
			if (product == null) {
				return BadRequest("Product cannot be null");
			}
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}
			var p = new ProductRepository();
			var newProduct = p.Save(product);
			if (newProduct == null)
			{
				return Conflict();
			}
			return Created(Request.RequestUri + newProduct.ProductId.ToString(), newProduct);
		}

        // PUT: api/Products/5
        public IHttpActionResult Put(int id, [FromBody]Product product)
        {
			if (product == null)
			{
				return BadRequest("Product cannot be null");
			}
			var p = new ProductRepository();
			var updatedProduct = p.Save(id, product);
			return Ok();
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
        }
    }
}
